﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace FlappyBird
{
    public class SpawnGround : MonoBehaviour
    {
        // Start is called before the first frame update
        public GameObject[] SkyObjects;
        public bool isGround;
        public Sprite[] sprites;
        GameObject currentSkyObject;
        void Start()
        {
            // if(isBackground)
            currentSkyObject = SkyObjects[0];
            float maxX = -100;
            while(maxX <= Screen.width)
            {
                GameObject obj = currentSkyObject.Spawn(transform);
                obj.GetComponent<Image>().sprite = sprites[GameManager.Instance.SkyIndex];
                float height = isGround ?  obj.GetComponent<RectTransform>().rect.height / 2 : -obj.GetComponent<RectTransform>().rect.height;
                float scale = Screen.height / Screen.width;

                obj.transform.localScale += new Vector3(0 , scale, 0);
                obj.transform.localPosition = new Vector3(maxX + obj.GetComponent<RectTransform>().rect.width, height, 0);
                maxX = obj.transform.localPosition.x;
            }
        }
        void SetBackground()
        {
            foreach (Transform child in transform)
            {
                print("slkdfjslkjdf");
            }
        }
    }
}
