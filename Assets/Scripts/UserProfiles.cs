﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace FlappyBird
{
    public class UserProfiles : Singleton<UserProfiles>
    {
        // Start is called before the first frame update
        void Start()
        {
            // PlayerPrefs.DeleteAll();
            if (!PlayerPrefs.HasKey("BestScore"))
            {
                PlayerPrefs.SetInt("BestScore", 0);
                PlayerPrefs.Save();
            }
        }

        public int GetBestScore()
        {
            return PlayerPrefs.GetInt("BestScore");
        }
        public void SetNewBestScore(int score)
        {
            PlayerPrefs.SetInt("BestScore", score);
            PlayerPrefs.Save();
        }
    }
}
