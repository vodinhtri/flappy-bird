﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace FlappyBird
{
    public class GameUtils : Singleton<GameUtils>
    {
        public bool Collision2Rect(Rect rect1, Rect rect2)
        {
            if (
                Mathf.Max(rect1.x, rect1.x + rect1.width / 2) < Mathf.Min(rect2.x, rect2.x - rect2.width / 2)
                || Mathf.Min(rect1.x, rect1.x - rect1.width / 2) > Mathf.Max(rect2.x, rect2.x + rect2.width / 2)
                || Mathf.Max(rect1.y, rect1.y + rect1.height / 2) < Mathf.Min(rect2.y, rect2.y - rect2.height / 2)
                || Mathf.Min(rect1.y, rect1.y - rect1.height / 2) > Mathf.Max(rect2.y, rect2.y + rect2.height / 2))
            {
                return false;
            }
            return true;
        }
    }
}
