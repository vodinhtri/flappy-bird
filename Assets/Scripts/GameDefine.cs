﻿using UnityEngine;

namespace FlappyBird
{
    [CreateAssetMenu(fileName = "GameDefine", menuName = "Flappy Bird/Game Defines")]
    public class GameDefine : ScriptableObject
    {
        public float GRAVITY = 1f;
        public float PLAYER_SPEED_X = 3f;
        public float PLAYER_SPEED_JUMP = 3f;
        public float OBS_SPAWN_FIRST = 2f;
        public float OBS_DISTANCE_SPAWN = 2f;
        public float OBS_SPAWN_DISTANCE_MAX = 7f;
        public float OBS_SPAWN_DISTANCE_MIN = -5f;
        public float CLOUD_SPEED = 2f;
        public int SCORE = 1;
        public int SCORE_MEDAL_1 = 1;
        public int SCORE_MEDAL_2 = 5;
        public int SCORE_MEDAL_3 = 10;
        public SoundAudioCLip[] AllSound;

    }
    [System.Serializable]
    public class SoundAudioCLip
    {
        public AudioManager.Sound sound;
        public AudioClip audioClip;
    }
}