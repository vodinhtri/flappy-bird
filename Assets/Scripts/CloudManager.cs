﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace FlappyBird
{
    public class CloudManager : MonoBehaviour
    {
        // Start is called before the first frame update
        public Sprite[] sprites;
        public bool isDecor;
        float speed;
        void Start()
        {
            speed = !isDecor ? GameManager.Instance.Defines.CLOUD_SPEED : GameManager.Instance.Defines.PLAYER_SPEED_X;
            foreach (Transform child in transform)
            {
                child.GetComponent<SpriteRenderer>().sprite = sprites[GameManager.Instance.SkyIndex];
            }
        }

        // Update is called once per frame
        void Update()
        {
            float step = speed * Time.deltaTime;
            foreach (Transform child in transform)
            {
                child.localPosition -= new Vector3(step, 0, 0);
                if (child.localPosition.x < -7f)
                {
                    child.localPosition = new Vector3(7, child.localPosition.y, 0);
                }
            }
        }
        public void StopMove()
        {
            speed = 0;
        }
    }
}
