﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace FlappyBird
{
    public class Obstacle : MonoBehaviour
    {
        // Start is called before the first frame update
        public bool isTop;
        float speed;
        bool canMove = true;
        bool canAddScore = true;
        GameObject player;
        void Start()
        {
            player = GameObject.FindWithTag("Player");
            speed = GameManager.Instance.Defines.PLAYER_SPEED_X;
            Sprite[] sprites = isTop ? GameManager.Instance.ObstacleManager.spritesTop : GameManager.Instance.ObstacleManager.spritesBottom;
            foreach (Transform child in transform)
            {
                child.GetComponent<SpriteRenderer>().sprite = sprites[GameManager.Instance.SkyIndex];
            }
        }
        public void Init()
        {
            canAddScore = true;
        }
        // Update is called once per frame
        void Update()
        {
            if (!canMove) return;
            float step = speed * Time.deltaTime;
            transform.localPosition -= new Vector3(step, 0, 0);
            if (transform.localPosition.x < GameManager.Instance.Defines.OBS_SPAWN_DISTANCE_MIN)
            {
                canMove = true;
                gameObject.Kill();
            }
            else if (transform.localPosition.x < player.transform.position.x && canAddScore && isTop)
            {
                canAddScore = false;
                GameManager.Instance.AddScore(GameManager.Instance.Defines.SCORE);
            }
        }
        public void StopMove()
        {
            canMove = false;
        }
    }
}
