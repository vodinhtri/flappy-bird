﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace FlappyBird
{
    public class UIManager : MonoBehaviour
    {
        // Start is called before the first frame update
        public Sprite[] bigNumber;
        public Sprite[] smallNumber;
        public Sprite[] medals;
        public GameObject ScoreContainer;
        public GameObject endScore;
        public GameObject endHighScore;
        public GameObject medal;
        public GameObject newHighText;
        public GameObject StarScreen;
        public GameObject EndScreen;

        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        public void ShowEndGame(int Score, int BestScore, bool isSetNew, int indexMedal = 0)
        {
            ScoreContainer.SetActive(false);
            EndScreen.SetActive(true);
            UpdateScore(Score, endScore);
            UpdateScore(BestScore, endHighScore);
            newHighText.SetActive(isSetNew);
            medal.GetComponent<Image>().sprite = medals[indexMedal];
        }
        public void ShowStartScreen(bool isShow)
        {
            StarScreen.SetActive(isShow);
        }
        public void ShowTutorial()
        {
            StarScreen.transform.GetChild(0).gameObject.SetActive(false);
            StarScreen.transform.GetChild(1).gameObject.SetActive(false);
            StarScreen.transform.GetChild(2).gameObject.SetActive(false);
            StarScreen.transform.GetChild(3).gameObject.SetActive(true);
        }
        public void HideTutorial()
        {
            StarScreen.transform.GetChild(0).gameObject.SetActive(false);
            StarScreen.transform.GetChild(1).gameObject.SetActive(false);
            StarScreen.transform.GetChild(2).gameObject.SetActive(false);
            StarScreen.transform.GetChild(3).gameObject.SetActive(false);
            ScoreContainer.SetActive(true);
        }
        public void UpdateScore(int score, GameObject obj = null)
        {
            if(obj == null ) obj = ScoreContainer;
            obj.transform.GetChild(0).gameObject.SetActive(false);
            obj.transform.GetChild(1).gameObject.SetActive(false);
            obj.transform.GetChild(2).gameObject.SetActive(false);
            if (score >= 100)
            {
                GameObject currentScoreUI = obj.transform.GetChild(2).gameObject;
                currentScoreUI.SetActive(true);
                Transform child = currentScoreUI.transform.GetChild(0);
                int number = (int)score/100;
                child.localScale = number == 1 ? new Vector3(2, 4, 1) : new Vector3(4, 4, 1);
                child.GetComponent<Image>().sprite = bigNumber[number];

                score = score % 100;
                number = (int)score/10;
                child = currentScoreUI.transform.GetChild(1);
                child.localScale = number == 1 ? new Vector3(2, 4, 1) : new Vector3(4, 4, 1);
                child.GetComponent<Image>().sprite = bigNumber[number];

                score = score % 10;
                number = (int)score;
                child = currentScoreUI.transform.GetChild(2);
                child.localScale = number == 1 ? new Vector3(2, 4, 1) : new Vector3(4, 4, 1);
                child.GetComponent<Image>().sprite = bigNumber[number];
            }
            else if (score >= 10 && score < 100)
            {
                GameObject currentScoreUI = obj.transform.GetChild(1).gameObject;
                currentScoreUI.SetActive(true);

                int number = (int)score/10;
                Transform child = currentScoreUI.transform.GetChild(0);
                child.localScale = number == 1 ? new Vector3(2, 4, 1) : new Vector3(4, 4, 1);
                child.GetComponent<Image>().sprite = bigNumber[number];

                score = score % 10;
                number = (int)score;
                child = currentScoreUI.transform.GetChild(1);
                child.localScale = number == 1 ? new Vector3(2, 4, 1) : new Vector3(4, 4, 1);
                child.GetComponent<Image>().sprite = bigNumber[number];

            }
            else if (score >= 0 && score < 10)
            {
                GameObject currentScoreUI = obj.transform.GetChild(0).gameObject;
                currentScoreUI.SetActive(true);

                int number = (int)score;
                Transform child = currentScoreUI.transform.GetChild(0);
                child.localScale = number == 1 ? new Vector3(2, 4, 1) : new Vector3(4, 4, 1);
                child.GetComponent<Image>().sprite = bigNumber[number];
            }
        }
    }
}
