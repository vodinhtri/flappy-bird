﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FlappyBird
{
    public class GameManager : Singleton<GameManager>
    {
        public enum GameState
        {
            RUNNING,
            PAUSED,
            ENDSCREEN
        }

        public PlayerController PlayerController;
        // public ItemManager ItemManager;
        public ObstacleManager ObstacleManager;
        public FloorManager FloorManager;
        public CloudManager CloudManager;
        public CloudManager DecorManager;
        public UIManager UIManager;
        public Events.EventGameChangeState OnGameStateChanged;
        public GameDefine Defines;
        public GameObject[] SystemPrefabs;

        public GameState CurrentGameState
        {
            get { return mCurrentGameState; }
            private set { mCurrentGameState = value; }
        }
        public bool IsTutorial
        {
            get { return mIsTutorial; }
        }
        public int Score
        {
            get { return mScore; }
        }
        public bool IsGameEnded = false;
        

        List<GameObject> mInstancedSystemPrefags;
        GameState mCurrentGameState = GameState.RUNNING;
        bool mIsTutorial = true;
        bool isShowTutorial = false;
        int mScore = 0;
        public int SkyIndex;

        protected override void Awake()
        {
            base.Awake();
            SkyIndex = Random.Range(0, 3);
        }

        void Start()
        {
            // if(KSE.Hub.Ingame.Instance != null)
            // {
            //     KSE.Hub.Ingame.Instance.Init(Pause, Resume);
            // }

            mInstancedSystemPrefags = new List<GameObject>();
            InstantiateSystemPrefabs();
            InitGame();
            StartGame();
        }
        void InitGame()
        {
            PlayerController.Init();
            // ObstacleManager.Init();
            UIManager.UpdateScore(mScore);
        }
        void Update()
        {
            if (mIsTutorial && isShowTutorial)
            {
                if (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))
                {
                    PlayStartSound();
                    HideTutorial();
                }
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                TogglePause();
            }
        }
        public void ShowTutorial()
        {
            if (mIsTutorial)
            {
                isShowTutorial = true;
                UIManager.ShowTutorial();
            }
            else
            {
                HideTutorial();
            }
        }
        public void HideTutorial()
        {
            isShowTutorial = false;
            mIsTutorial = false;
            UIManager.HideTutorial();
            ObstacleManager.Init();
        }
        protected override void OnDestroy()
        {
            base.OnDestroy();

            for (int i = 0; i < mInstancedSystemPrefags.Count; i++)
            {
                Destroy(mInstancedSystemPrefags[i]);
            }

            mInstancedSystemPrefags.Clear();
        }

        public void StartGame()
        {
            mIsTutorial = StaticClass.IsTutorial;
            if (mIsTutorial)
            {
                UIManager.ShowStartScreen(StaticClass.IsTutorial);
            }
            else
            {
                HideTutorial();
            }
            SetState(GameState.RUNNING);

        }

        public void RestartGame()
        {
            StaticClass.IsTutorial = false;
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
        public void StopMove()
        {
            ObstacleManager.StopMove();
            FloorManager.StopMove();
            CloudManager.StopMove();
            DecorManager.StopMove();
        }

        public void QuitGame()
        {
            SetState(GameState.ENDSCREEN);
        }

        void Pause()
        {
            SetState(GameState.PAUSED);
        }

        void Resume()
        {
            SetState(GameState.RUNNING);
        }

        public void TogglePause()
        {
            if (mCurrentGameState != GameState.RUNNING && mCurrentGameState != GameState.PAUSED)
            {
                return;
            }

            SetState(mCurrentGameState == GameState.RUNNING ? GameState.PAUSED : GameState.RUNNING);
        }

        void InstantiateSystemPrefabs()
        {
            GameObject prefabInstance;

            for (int i = 0; i < SystemPrefabs.Length; i++)
            {
                prefabInstance = Instantiate(SystemPrefabs[i]);
                mInstancedSystemPrefags.Add(prefabInstance);
            }
        }
        public void PlayStartSound()
        {
            AudioManager.PlaySound(AudioManager.Sound.Button);
        }
        public void AddScore(int score)
        {
            AudioManager.PlaySound(AudioManager.Sound.Score);
            mScore += score;
            UIManager.UpdateScore(mScore);
        }
        int SetAchievement(int Score)
        {
            int result = 0;
            if (Score >= Defines.SCORE_MEDAL_3) result = 2;
            else if (Score < Defines.SCORE_MEDAL_3 && Score >= Defines.SCORE_MEDAL_2) result = 1;
            return result;
        }
        void SetState(GameState state)
        {
            // GameState previousGameState = mCurrentGameState;
            mCurrentGameState = state;

            switch (state)
            {
                case GameState.RUNNING:
                    Time.timeScale = 1.0f;
                    break;
                case GameState.PAUSED:
                    Time.timeScale = 0.0f;
                    break;
                case GameState.ENDSCREEN:
                    int bestScore = UserProfiles.Instance.GetBestScore();
                    int achievement = SetAchievement(mScore);
                    bool isNew = mScore > bestScore;
                    if (mScore > bestScore)
                    {
                        UserProfiles.Instance.SetNewBestScore(mScore);
                        bestScore = mScore;
                    }

                    UIManager.ShowEndGame(mScore, bestScore, isNew, achievement);
                    break;
                default:
                    break;
            }
            // if (mCurrentGameState != previousGameState)
            // {
            //     OnGameStateChanged.Invoke(mCurrentGameState, previousGameState);
            // }
        }
    }
}