﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace FlappyBird
{
    public class FloorManager : MonoBehaviour
    {
        // Start is called before the first frame update
        public GameObject floor;
        public float speed = 2f;
        public Sprite[] sprites;
        public Sprite[] sprites2;
        bool canMove = true;
        void Start()
        {
            speed = GameManager.Instance.Defines.PLAYER_SPEED_X;
            float width = floor.GetComponent<Renderer>().bounds.size.x;
            float maxX = -width;
            float check = -100;
            while (check < 2 *Screen.width)
            {
                GameObject obj = floor.Spawn(transform);
                obj.GetComponent<SpriteRenderer>().sprite = sprites[GameManager.Instance.SkyIndex];
                obj.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = sprites2[GameManager.Instance.SkyIndex];
                obj.transform.localPosition += new Vector3(maxX + width, 0, 0);
                maxX = obj.transform.localPosition.x;
                check = Camera.main.WorldToScreenPoint(obj.transform.localPosition).x;
            }
        }

        // Update is called once per frame
        void Update()
        {
            if(!canMove) return;
            float step = speed * Time.deltaTime;
            foreach (Transform child in transform)
            {
                child.localPosition -= new Vector3(step, 0, 0);
            }
            foreach (Transform child in transform)
            {
                float posX = Camera.main.WorldToScreenPoint(child.position).x;
                if (posX <= -100)
                {
                    GameObject obj = GetLastObject();
                    child.localPosition = new Vector2(obj.transform.localPosition.x + obj.GetComponent<Renderer>().bounds.size.x, child.transform.localPosition.y);
                }
            }
        }
        GameObject GetLastObject()
        {
            GameObject obj = transform.GetChild(0).gameObject;
            foreach (Transform child in transform)
            {
                if (child.localPosition.x > obj.transform.localPosition.x)
                    obj = child.gameObject;
            }
            return obj;
        }
        public void StopMove()
        {
            canMove = false;
        }
    }
}
