﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace FlappyBird
{
    public class Events : MonoBehaviour
    {
        [Serializable]
        public class EventGameChangeState : UnityEvent<GameManager.GameState, GameManager.GameState> {}
        [Serializable]
        public class EventPlayerFrenzy : UnityEvent<bool> {}
    }
}