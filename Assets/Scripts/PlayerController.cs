﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace FlappyBird
{
    public class PlayerController : MonoBehaviour
    {
        public enum PlayerState
        {
            IDLE,
            JUMP_UP,
            MOVE_DOWN,
            HIT,
            DIE,
        }
        public PlayerState state;
        GameObject[] Obstacles;
        Vector2 moveDirection;
        Vector2 rectSize;
        Rect playerRect;
        Animator animator;
        float jumpSpeed;
        float gravity;
        bool canCheck = true;
        void Start()
        {

        }
        public void Init()
        {
            moveDirection = new Vector2();
            jumpSpeed = GameManager.Instance.Defines.PLAYER_SPEED_JUMP;
            gravity = GameManager.Instance.Defines.GRAVITY;
            rectSize = transform.GetChild(0).GetComponent<Renderer>().bounds.size / 2;
            animator = transform.GetChild(0).GetComponent<Animator>();
        }
        public void SetState(PlayerState newState)
        {
            state = newState;
            switch (state)
            {
                case PlayerState.IDLE:
                case PlayerState.JUMP_UP:
                    moveDirection.y = jumpSpeed;
                    break;
                case PlayerState.MOVE_DOWN:
                case PlayerState.HIT:
                    AudioManager.PlaySound(AudioManager.Sound.Hit);
                    StartCoroutine(DelayedEvents(() =>
                    {
                        AudioManager.PlaySound(AudioManager.Sound.Fall);
                    }, 0.1f));
                    animator.SetTrigger("isHit");
                    GameManager.Instance.IsGameEnded = true;
                    StartCoroutine(SetDie());
                    break;
                case PlayerState.DIE:
                    GameManager.Instance.QuitGame();
                    break;
            }
        }
        IEnumerator SetDie()
        {
            yield return new WaitForSeconds(1f);
            SetState(PlayerState.DIE);
        }
        void Update()
        {
            if (GameManager.Instance.IsTutorial) return;
            switch (state)
            {
                case PlayerState.IDLE:
                    break;
                case PlayerState.JUMP_UP:
                    UpdateRotation();
                    Movement();
                    break;
                case PlayerState.MOVE_DOWN:
                case PlayerState.HIT:
                    Movement();
                    return;
                case PlayerState.DIE:
                    Movement();
                    break;
            }
            TouchHandler();
        }
        private void FixedUpdate()
        {
            if (GameManager.Instance.IsTutorial) return;
            if (canCheck) CheckCollision();
        }
        void TouchHandler()
        {
            if (state == PlayerState.HIT || GameManager.Instance.IsGameEnded) return;
            if (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))
            {
                JumpUp();
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                JumpUp();
            }
        }
        void JumpUp()
        {
            AudioManager.PlaySound(AudioManager.Sound.Flap);
            SetState(PlayerState.JUMP_UP);
        }
        void Movement()
        {
            // if (!canCheck) return;
            moveDirection.y -= gravity * Time.deltaTime;
            // transform.Translate(moveDirection);
            Vector3 check = transform.position + (Vector3)moveDirection;
            Vector2 tmpPos = Camera.main.WorldToScreenPoint(check);
            if (tmpPos.y < Screen.height && transform.position.y > -2f)
            {
                transform.position += (Vector3)moveDirection;
            }


        }
        void OnDrawGizmosSelected()
        {
            Gizmos.color = new Color(1, 1, 0, 0.75F);
            Vector2 size = transform.GetChild(0).GetComponent<Renderer>().bounds.size / 2;
            Gizmos.DrawWireCube((Vector3)transform.position, (Vector3)size);
        }
        void CheckCollision()
        {
            if (!canCheck) return;
            Obstacles = GameObject.FindGameObjectsWithTag("Obstacle");
            Vector2 pos = transform.position;

            playerRect = new Rect(pos.x, pos.y, rectSize.x, rectSize.y);
            foreach (GameObject obstacle in Obstacles)
            {
                Vector2 obstacleSize = obstacle.GetComponent<Renderer>().bounds.size;
                Vector2 obstaclePos = obstacle.transform.position;

                Rect obstacleRect = new Rect(obstaclePos.x, obstaclePos.y, obstacleSize.x, obstacleSize.y);
                if (GameUtils.Instance.Collision2Rect(playerRect, obstacleRect))
                {
                    canCheck = false;
                    GameManager.Instance.StopMove();
                    foreach (Transform effect in transform.GetChild(1))
                    {
                        Vector2 effectSize = effect.GetComponent<Renderer>().bounds.size;
                        Vector2 effectPos = effect.position;

                        if (state != PlayerState.HIT && state != PlayerState.DIE) SetState(PlayerState.HIT);
                        Rect effctRect = new Rect(effectPos.x, effectPos.y, effectSize.x, effectSize.y);
                        if (GameUtils.Instance.Collision2Rect(obstacleRect, effctRect))
                        {
                            effect.gameObject.SetActive(true);
                            return;
                        }
                    }
                    return;
                }
            }
        }
        void UpdateRotation()
        {
            float degrees = Mathf.Atan(moveDirection.y / (GameManager.Instance.Defines.PLAYER_SPEED_X / 10));
            degrees = degrees * (180 / Mathf.PI);
            transform.GetChild(0).rotation = Quaternion.Euler(Vector3.forward * degrees);

        }
        IEnumerator DelayedEvents(System.Action ev, float time)
        {
            yield return new WaitForSeconds(time);
            ev();
        }
    }
}
