﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnDrawGizmosSelected()
    {
        // if (!groundCheckPosition)
        //     return;
        Gizmos.color = new Color(1, 1, 0, 0.75F);
        Vector2 size = GetComponent<Renderer>().bounds.size;
        Gizmos.DrawWireCube((Vector3)transform.position, (Vector3)size);
    }
}
