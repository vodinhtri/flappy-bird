﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace FlappyBird
{
    public class ObstacleManager : MonoBehaviour
    {
        public GameObject[] blockTop;
        public GameObject[] blockBottom;
        public Sprite[] spritesTop;
        public Sprite[] spritesBottom;

        GameObject lastObject;

        public void Init()
        {
            SpawnObstacles();
        }
        void SpawnObstacles()
        {
            float SpawnX = GameManager.Instance.Defines.OBS_SPAWN_FIRST;
            while (SpawnX < GameManager.Instance.Defines.OBS_SPAWN_DISTANCE_MAX)
            {
                int randID = Random.Range(0, blockTop.Length);
                GameObject blTop = blockTop[randID].Spawn(transform.GetChild(0));
                blTop.transform.localPosition = new Vector3(SpawnX, 4.5f, 0);

                GameObject blBot = blockBottom[blockBottom.Length - 1 - randID].Spawn(transform.GetChild(1));
                blBot.transform.localPosition = new Vector3(SpawnX, -1.65f, 0);

                lastObject = blBot;
                SpawnX += GameManager.Instance.Defines.OBS_DISTANCE_SPAWN;
            }
        }
        // Update is called once per frame
        void Update()
        {
            if(GameManager.Instance.IsTutorial) return;
            if (lastObject.transform.localPosition.x <= GameManager.Instance.Defines.OBS_SPAWN_DISTANCE_MAX)
            {
                lastObject = SpawnNewObstacle();
            }
        }
        GameObject SpawnNewObstacle()
        {
            int randID = Random.Range(0, blockTop.Length);
            GameObject blTop = blockTop[randID].Spawn(transform.GetChild(0));
            blTop.transform.localPosition = new Vector3(lastObject.transform.localPosition.x + GameManager.Instance.Defines.OBS_DISTANCE_SPAWN, 4.5f, 0);
            blTop.GetComponent<Obstacle>().Init();

            GameObject blBot = blockBottom[blockBottom.Length - 1 - randID].Spawn(transform.GetChild(1));
            blBot.transform.localPosition = new Vector3(lastObject.transform.localPosition.x + GameManager.Instance.Defines.OBS_DISTANCE_SPAWN, -1.65f, 0);
            blBot.GetComponent<Obstacle>().Init();

            return blTop;
        }
        public void StopMove()
        {
            foreach (Transform child in transform.GetChild(0))
            {
                child.GetComponent<Obstacle>().StopMove();
            }
            foreach (Transform child in transform.GetChild(1))
            {
                child.GetComponent<Obstacle>().StopMove();
            }
        }
    }
}
