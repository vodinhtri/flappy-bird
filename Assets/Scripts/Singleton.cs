﻿using UnityEngine;

namespace FlappyBird
{
    public class Singleton<T> : MonoBehaviour where T : Singleton<T>
    {
        private static T instance;
        public static T Instance
        {
            get { return instance; }
        }

        public static bool IsInstantiated
        {
            get { return instance != null; }
        }

        protected virtual void Awake()
        {
            if(instance != null)
            {
                Debug.LogError("[Singleton] Instance already exist.");
            }
            else
            {
                instance = this as T;
            }
        }

        protected virtual void OnDestroy()
        {
            if(instance == this)
            {
                instance = null;
            }
        }
    }
}
