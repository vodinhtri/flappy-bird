﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace FlappyBird
{
    public static class AudioManager 
    {
        public enum Sound
        {
            Button,
            Fall,
            Flap,
            Hit,
            Jingle,
            Score
        }
        public static void PlaySound(Sound sound)
        {
            GameObject soundGameObject = GameObject.Find("AudioManager");
            AudioSource audioSource = soundGameObject.GetComponent<AudioSource>();
            audioSource.PlayOneShot(GetAudioClip(sound));
        }
        static AudioClip GetAudioClip(Sound sound){
            foreach(SoundAudioCLip soundAudioCLip in GameManager.Instance.Defines.AllSound)
            {
                if(soundAudioCLip.sound == sound)
                {
                    return soundAudioCLip.audioClip;
                }
            }
            Debug.LogError("Sound " + sound + " not found!");
            return null;
        }
        // Start is called before the first frame update
    }
}
